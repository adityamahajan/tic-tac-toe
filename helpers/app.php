<?php

use Venjoy\Pedy\Container;

function app($className = '') {
    static $app;
    if (!$app) {
        $app = new Container;
    }
    return $className ? $app->get($className) : $app;
}