<?php 
use App\Services\EventService;
use App\Path;
use App\Events\PathParsed;
use App\Listeners\BuildNewPath;

function event($event) {
    
    $eventClass = get_class($event);
    // bindings
    $bindings = app(EventService::class)->bindings;
    $listeners = $bindings[$eventClass];
    foreach ($listeners as $listener) {
        app($listener)->handle($event);
    }
}