<?php

namespace App;

use PHPUnit\Framework\TestCase;

class changePathTest extends TestCase {
    /**
     * @var changePath
     */
    public $change;
    public $allpaths;
    public $allindex;

    public function setup() {
        parent::setup();
        $this->change = app(changePath::class);
        $this->allpaths = [[0,1], [2,3], [4,5,6], [1,7,2], [4,6,2,5]];
        $this->allindex = [0,0,0,-1];
    }

    /**
     * @test
     */
    public function moves_to_end() {
        $this->change->movetoend($this->allpaths, $this->allindex);
        dd($this->allpaths);
        //var_dump($this->allindex);
    }
    /*public function finds_new_path() {
        $this->change->change($this->gamemoves, $this->index);
        var_dump($this->gamemoves);
        dd($this->index);
    }*/
}