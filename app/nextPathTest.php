<?php

namespace App;

use PHPUnit\Framework\TestCase;

class nextPathTest extends TestCase {
    /**
     * @var nextPath
     */
    public $move;
    public $path;
    public $index;

    public function setup() {
        parent::setup();
        $this->move = app(nextPath::class);
        $this->path = [2,0,3,1,4,5,7];
        $this->index = [0,0];
    }

    /**
     * @test
     */
    public function returns_array_with_position_and_indexes() {
        dd($this->move->getpath($this->path,$this->index));
    }
}