<?php 

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\availablePos;

class availablePosTest extends TestCase{
    /**
     * @var availablePos
     */
    public $obj;
    public function setup(){
        parent::setup();
        $this->obj = new availablePos([3,4,5]);
    }
    /**
     * @test
     */
    public function returns_available_positions(){
        $this->assertTrue($this->obj->positions() == [0,1,2,6,7,8]);
    }
}