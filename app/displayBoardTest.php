<?php

namespace App;
use PHPUnit\Framework\TestCase;

class displayBoardTest extends TestCase {
    /**
     * @var displayBoard
     */
    public $display;
    public function setup() {
        parent::setup();
        $this->display = app(displayBoard::class);
    }
    /**
     * @test
     */
    public function displays_board() {
        $this->display->display();
    }
}