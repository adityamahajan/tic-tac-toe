<?php 

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    /**
     * @var Board
     */
    public $board;

    public function setUp() {
        parent::setUp();
        $this->board = new Board;
        $this->board->init(3);
    }

    /**
     * @test
     */
    public function it_should_init_with_null() {
       $this->assertTrue($this->board->pos(0,0) == null); 
    }

    /**
     * @test
     */
    public function it_should_give_available_pos() {
        
        dd($this->board->availablePos());
        
    }
}