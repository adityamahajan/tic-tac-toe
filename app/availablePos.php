<?php

namespace App;

class availablePos {
    
    public function positions($pathData){
        $default = [0,1,2,3,4,5,6,7,8];
        return array_merge(array_diff($default,$pathData), array_diff($pathData,$default));
    }
}