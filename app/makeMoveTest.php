<?php

namespace App;

use PHPUnit\Framework\TestCase;

class makeMoveTest extends TestCase {
    
    /**
     * @var makeMove
     */
    public $move;
    public $gamemoves;
    public function setup() {
        parent::setup();
        $this->move = app(makeMove::class);
        $this->gamemoves = [];
    }
    
    /**
     * @test
     */
    public function it_returns_position_from_saved_path() {
        $this->move->move($this->gamemoves);
    }
}