<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Game;

class GameTest extends TestCase{

    /**
     * @var Game
     */
    public $game;

    public function setUp() {
        parent::setUp();
        $this->game = app(Game::class);
    }
    /**
     * @test
     */
    public function it_should_return_various_paths() {
        $this->assertTrue($this->game->play());
    }
}