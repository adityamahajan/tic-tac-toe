<?php

namespace App;

class GameStatus {

    public function status($arr){
        $board = ['0','0','0','0','0','0','0','0','0'];
        $y = count($arr);
        //var_dump($arr);
        for($x = 0; $x < $y; $x += 2){
            $board[$arr[$x]] = 'X';
        }
        for($x = 1; $x < $y; $x += 2){
            $board[$arr[$x]] = 'O';
        }
    
        if ($board[0] && $board[0] == $board[4] && $board[4] == $board[8])
            return $board[0];
                
        if ($board[2] && $board[2] == $board[4] && $board[4] == $board[6])
            return $board[2];
            
        for($x = 0; $x < 7; $x += 3){
            if ($board[$x] && $board[$x] == $board[$x+1] && $board[$x+1] == $board[$x+2])
                return $board[$x];	
        }
        for($x = 0; $x < 3; $x++){
            if ($board[$x] && $board[$x] == $board[$x+3] && $board[$x+3] == $board[$x+6])
                return $board[$x];	
        }
        return 'n';
    } 
}