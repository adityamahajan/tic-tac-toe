<?php 

namespace App\Events;

class PathParsed {
    public $path;

    public function __construct($path) {
        $this->path = $path;
    }
}