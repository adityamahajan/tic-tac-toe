<?php 

namespace App\Services;

use App\Events\PathParsed;
use App\Listeners\BuildNewPath;


class EventService {
    
    public $bindings = [
        PathParsed::class => [
            BuildNewPath::class,
        ]
    ];
    
}