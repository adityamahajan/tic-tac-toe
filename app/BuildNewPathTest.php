<?php

namespace App;

use PHPUnit\Framework\TestCase;
use App\Events\PathParsed;
use App\Path;
use App\Listeners\BuildNewPath;
use App\GameStatus;
use App\availablePos;
use App\saveWinner;

class BuildNewPathTest extends TestCase{
    
    /**
     * @var BuildNewPath
     */
    public $build;
    public $event;
    public $path;
    public $status;
    public $posarr;
    public $win;
    public function setup(){
        parent::setup();
        $this->status = app(GameStatus::class);
        $this->posarr = app(availablePos::class);
        $this->win = app(saveWinner::class);
        //$this->build = new BuildNewPath($this->status, $this->win, $this->posarr);
        $this->build = app(BuildNewPath::class);
        $this->path = new Path;
        $this->path->data = [8];
        $this->event = new PathParsed($this->path);
    }
    /**
     * @test
     */
    public function gives_new_path(){
        $this->build->handle($this->event);
    }
}