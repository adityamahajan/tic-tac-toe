<?php 

namespace App;

class Board {
    public $board = [];

    public function init($size) {
        $j = pow($size,2);
        for ($i = 0; $i < $j; $i++) {
                $this->board[$i] = null; 
        }
    }

    public function pos($x) {
        return $this->board[$x];
    }

}