<?php
 
namespace App;

class makeMove {
    public $opath;
    public $availpos;
    public $status;
    public $change;
    public function __construct(nextPath $path, availablePos $availpos, GameStatus $status, changePath $change) {
        $this->opath = $path;
        $this->availpos = $availpos; 
        $this->status = $status;  
        $this->change = $change;           
    }
    public function move($gamemoves) {
        $index = 0;
        $oindex = array_fill(0,4,-1);
        echo "<pre>";
        while(count($gamemoves) < 8 && $this->status->status($gamemoves) != 'O') {
            $xpos = $this->availpos->positions($gamemoves);
            $gamemoves = array_merge([], $gamemoves, [$xpos[array_rand($xpos)]]);
            $opos = $this->opath->getpath($gamemoves,$index);
            if($opos > -1) {
                $gamemoves = array_merge([], $gamemoves, [$opos]);
                $oindex[(count($gamemoves)/2)-1] = $index;
            }
            else {
                if($this->status->status($gamemoves) == 'X' || count($gamemoves) < 6) {
                    $json_win = json_decode(file_get_contents('win.json'), true);
                    $this->change->movetoend($json_win, $oindex);
                    file_put_contents('win.json',json_encode($json_win));
                }
                break;
            }
        }
        var_dump($gamemoves);
    }
}