<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\saveWinner;

class saveWinnerTest extends TestCase {
    /**
     * @var saveWinner
     */
    public $win;
    public $count;
    public $data;
    public function setup() {
        parent::setup();
        $this->win = new saveWinner;
        $this->count = 4;
        $this->data = [0,3,1,4,2];
    }
    /**
     * @test
     */
    public function returns_if_winner_is_found() {
        $this->assertTrue($this->win->save('X', $this->data, $this->count));
    }
}