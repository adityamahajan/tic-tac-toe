<?php 

namespace App\Listeners;

use App\Events\PathParsed;
use App\Path;
use App\GameStatus;
use App\saveWinner;
use App\availablePos;

class BuildNewPath {

    public $status;
    public $win;
    public $posarr;
    public function __construct(GameStatus $status, saveWinner $win, availablePos $posarr) {
        $this->status = $status;
        $this->win = $win;
        $this->posarr = $posarr;
    }
    public function handle(PathParsed $event) {
                
        $w = $this->status->status($event->path->data);
        $end = $this->win->save($w, $event->path->data, count($event->path->data));

        if($end != true){
            foreach($this->posarr->positions($event->path->data) as $pos){
                // array_push($event->path->data, $pos);
                $p1 = new Path;
                $p1->data = array_merge([], $event->path->data, [$pos]);
                event(new PathParsed($p1));
            }
        }
    }
}