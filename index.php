<?php

use App\Game;

require_once __DIR__ . '/vendor/autoload.php';

session_start();
set_time_limit(0);
if(!isset($_SESSION['Game']))
    $_SESSION['Game'] = app(Game::class);
?>

<html>
    <head>
        <title>
            Tic Tac Toe
        </title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <h1>Let's Play</h1>
        <?php $_SESSION['Game']->play(); ?>
    </body>
</html>